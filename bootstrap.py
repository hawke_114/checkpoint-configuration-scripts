import configparser
import argparse
from bgp import bgp_generate

parser = argparse.ArgumentParser(description='Generates Checkpoint bootstrap config')
parser.add_argument('--save', default=False, action='store_true')
parser.add_argument('--config', nargs=1)
arguments = parser.parse_args()

configfile = arguments.config
config = configparser.ConfigParser()
config.read(configfile)


customer = config['General']['Customer']
site = config['General']['site']
dns1 = config['General']['DNS1']
dns2 = config['General']['DNS2']


wan_section = 'Primary_WAN_Interfaces'
sec_wan_section = 'Secondary_WAN_Interfaces'

interface_section = 'Primary_Interfaces'
sec_interface_section = 'Secondary_Interfaces'
vlan_section = 'Primary_LAN1_VLAN'
sec_vlan_section = 'Secondary_LAN1_VLAN'
enable_bgp = False
if config['General'].getboolean('ANAP_BGP') is True:
    enable_bgp = True



def fw_config(*args, **kwargs):
    command_list = list()
    networks = list()

    def save(num):
        with open(f'{customer}-{site}-FW{num}.txt', 'w') as config_file:
            for items in command_list:
                config_file.write(items + '\n')



    if args[0] == 'primary':
        command_list.append('\n\n\n\n####################FW1##################################################\n\n\n\n')
        command_list.append(f'set device-details hostname {site}-FW1')
        command_list.append(f'set dns primary ipv4-address {dns1} secondary ipv4-address {dns2}')
        if config.has_section(interface_section):
            for key in config[interface_section]:
                if config.has_option(interface_section,key):
                    interface = str(key)
                    value = config[interface_section][key]
                    prefix = value.split('/')
                    command_list.append(f'set interface {key.upper()} state on')
                    command_list.append(f'set interface {key.upper()} ipv4-address {prefix[0]} mask-length {prefix[1]}')
                    networks.append(value)

        if config.has_section(wan_section):
            for key in config[wan_section]:
                if config.has_option(wan_section,key):
                    interface = str(key)
                    value = config[wan_section][key]
                    prefix = value.split('/')
                    command_list.append(f'set interface {key.upper()} state on')
                    command_list.append(f'add internet-connection interface {key.upper()} type static ipv4-address '
                                        f'{prefix[0]} mask-length {prefix[1]} default-gw 169.254.10.3') 
                    networks.append(value)

        vlan_section = 'Primary_LAN1_VLAN'
        if config.has_section(vlan_section):
            for key in config[vlan_section]:
                if config.has_option(vlan_section,key):
                    value = config[vlan_section][key]
                    prefix = value.split('/')
                    command_list.append(f'add interface LAN1 vlan {key} ipv4-address {prefix[0]} mask-length {prefix[1]}')
                    networks.append(value)
        if kwargs['bgp_enable'] is True:
            bgp_commands = bgp_generate(config['ANAP_BGP']['LOCAL_AS'], config['ANAP_BGP']['PEER_AS'],
                                    config['ANAP_BGP']['PEER_IP'],
                                    outfilter=networks)
            command_list.extend(bgp_commands)
        if args[1] is True:
            save(1)

    if args[0] == 'secondary':
        command_list.append('\n\n\n\n####################FW2##################################################\n\n\n\n')
        command_list.append(f'set device-details hostname {site}-FW2')
        command_list.append(f'set dns primary ipv4-address {dns1} secondary ipv4-address {dns2}')
        for key in config[interface_section]:
            if config.has_option(interface_section,key):
                value = config[sec_interface_section][key]
                pri_value = config[interface_section][key]
                prefix = value.split('/')
                pri_prefix = pri_value.split('/')
                command_list.append(f'set interface {key.upper()} state on')
                command_list.append(f'set interface {key.upper()} ipv4-address {prefix[0]} mask-length {pri_prefix[1]}')
                networks.append(pri_value)

        if config.has_section(sec_wan_section):
            for key in config[sec_wan_section]:
                if config.has_option(sec_wan_section,key):
                    interface = str(key)
                    value = config[sec_wan_section][key]
                    pri_value = config[wan_section][key]
                    prefix = value.split('/')
                    pri_prefix = pri_value.split('/')
                    command_list.append(f'set interface {key.upper()} state on')
                    command_list.append(f'add internet-connection interface {key.upper()} type static ipv4-address '
                                        f'{prefix[0]} mask-length {pri_prefix[1]} default-gw 169.254.10.3')
                    networks.append(value)

        vlan_section = 'Primary_LAN1_VLAN'
        if config.has_section(sec_vlan_section):
            for key in config[sec_vlan_section]:
                if config.has_option(vlan_section, key):
                    value = config[sec_vlan_section][key]
                    pri_value = config[vlan_section][key]
                    prefix = value.split('/')
                    pri_prefix = pri_value.split('/')
                    command_list.append(f'add interface LAN1 vlan {key} '
                                        f'ipv4-address {prefix[0]} mask-length {pri_prefix[1]}')
                    networks.append(pri_value)
        if kwargs['bgp_enable'] is True:
            bgp_commands = bgp_generate(config['ANAP_BGP']['LOCAL_AS'], config['ANAP_BGP']['PEER_AS'],
                                        config['ANAP_BGP']['PEER_IP'],
                                        outfilter=networks)
            command_list.extend(bgp_commands)

        if args[1] is True:
            save(2)

    for commands in command_list:
        print(commands)


fw_config('primary', arguments.save, bgp_enable=enable_bgp)

if config.has_option('General', 'HA') and config['General'].getboolean('HA') is True:
    fw_config('secondary', arguments.save, bgp_enable=enable_bgp)
