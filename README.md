# Checkpoint-configuration-scripts

## Installation

### Dependencies

 - [X] Python3
 - [X] ipcalc
 - [X] six

Dependencies are located in requirements.txt

Run the following command to install:

```shell
pip install -r requirements.txt
```

## Usage

### bootstrap.py
 - ingests .ini file specified by --config parameter,
parses the configuration items and outputs a checkpoint bootstrap config
 - input .ini file name can be changed in bootstrap.py
 - runs standalone or with the argument '--save'
   - if 
   - ```--save``` is specified, output files created with the pattern <customer>-<site>-FW-<1|2>.txt
#### Currently supported configurations:

Un-checked items are planned
- [X] ANAP BGP
- [X] DNS
- [X] Hostname
- [X] Physical Interface Configuration
- [X] VLAN Interface Configuration
- [ ] Customer Router BGP
- [ ] Route redistribution
- [ ] DHCP Server configuration

#### Example

```shell
python bootstrap.py --config bootstrap_sample.ini
```

or

```shell
python bootstrap.py --save --config bootstrap_sample.ini
```

### dhcp_reservation.py

 - runs standalone
 - ingests reservations.csv and outputs to the terminal as well as a file called dhcp_reservation_commands in the
current working directory

### static_route.py

 - runs standalone
 - ingests static_routes.csv and outputs to terminal as well as a file called route_commands.txt in the current
working directory


## Purpose

Easily generate various configuration commands for checkpoint firewalls


## Contributing

Feel free to request a feature or change or submit a PR
