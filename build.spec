# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


bootstrap_a = Analysis(
    ['bootstrap.py'],
    pathex=[],
    binaries=[],
    datas=[('bootstrap_sample.ini', 'samples')],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)

dhcp_a = Analysis(
    ['dhcp_reservation.py'],
    pathex=[],
    binaries=[],
    datas=[('reservations.csv', 'samples')],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)

static_a = Analysis(
    ['static_route.py'],
    pathex=[],
    binaries=[],
    datas=[('static_routes.csv', 'samples')],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)

MERGE( (bootstrap_a, 'bootstrap', 'bootstrap'), (static_a, 'static', 'static'), (dhcp_a, 'dhcp', 'dhcp'), )

bootstrap_pyz = PYZ(bootstrap_a.pure, bootstrap_a.zipped_data, cipher=block_cipher)

bootstrap_exe = EXE(
    bootstrap_pyz,
    bootstrap_a.scripts,
    [],
    exclude_binaries=True,
    name='bootstrap',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
)
bootstrap_coll = COLLECT(
    bootstrap_exe,
    bootstrap_a.binaries,
    bootstrap_a.zipfiles,
    bootstrap_a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name='bootstrap',
)

static_pyz = PYZ(static_a.pure, static_a.zipped_data, cipher=block_cipher)

static_exe = EXE(
    static_pyz,
    static_a.scripts,
    [],
    exclude_binaries=True,
    name='static_route',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
)

static_coll = COLLECT(
    static_exe,
    static_a.binaries,
    static_a.zipfiles,
    static_a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name='static_route',
)

dhcp_pyz = PYZ(dhcp_a.pure, dhcp_a.zipped_data, cipher=block_cipher)

dhcp_exe = EXE(
    dhcp_pyz,
    dhcp_a.scripts,
    [],
    exclude_binaries=True,
    name='dhcp_reservation',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
)
dhcp_coll = COLLECT(
    dhcp_exe,
    dhcp_a.binaries,
    dhcp_a.zipfiles,
    dhcp_a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name='dhcp_reservation',
)
