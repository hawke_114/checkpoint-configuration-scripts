import csv
import os
import sys

filename = 'route_commands.txt'

with open(sys.argv[1], newline='') as csvfile:
    routereader = csv.reader(csvfile, delimiter=',', quotechar='|')
    with open(filename, 'w') as output:
        print('writing commands to file...\n')
        for row in routereader:
            try:
                command_string = f'add static-route destination {row[0]} nexthop gateway ipv4-address {row[1]}'
                print(command_string)
                output.write(command_string + '\n')
            except:
                errormsg = '\n#CSV PARSING ERROR: Remove any blank lines and try again'
                print(errormsg)
                output.write(errormsg)
                exit(1)




print(f'\nSUCCESS! Commands written to {os.path.abspath(filename)}')











