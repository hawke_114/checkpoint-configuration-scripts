import ipcalc

def bgp_generate(asn, peer_asn, peer_ip, **kwargs):
    command_list = list()
    command_list.append(f'set as {asn}')
    command_list.append(f'set bgp external remote-as {peer_asn} on')
    command_list.append(f'set bgp external remote-as {peer_asn} peer {peer_ip} on')
    command_list.append(f'set bgp external remote-as {peer_asn} peer {peer_ip} route-refresh on')
    for addresses in kwargs['outfilter']:
        mask_bits = addresses.split('/')[1]
        network = str(ipcalc.Network(addresses).network())
        command_list.append(f'set routemap BGP-OUT id 2 match network {network}/{mask_bits} exact restrict off')
    command_list.append('set routemap BGP-OUT id 2 allow')
    command_list.append(f'set bgp external remote-as {peer_asn} export-routemap BGP-OUT preference 1 on')
    command_list.append(f'set bgp external remote-as {peer_asn} import-routemap BGP-IN preference 1 on')
    return command_list

'''
if __name__ == "__main__":
    import configparser

    config = configparser.ConfigParser(allow_no_value=True)
    config.read('bootstrap_sample.ini')

    interfaces = list()
    out_filter = list()
    for keys in config['Primary_Interfaces']:
        interfaces.append(config['Primary_Interfaces'][keys])
    for keys in config['Primary_LAN1_VLAN']:
        interfaces.append(config['Primary_LAN1_VLAN'][keys])



commands = bgp_generate(65000, 56278, '192.168.1.1', outfilter=interfaces)

for command in commands:
    print(command)
'''


