import csv
import os

filename = 'dhcp_reservation_commands.txt'

with open('reservations.csv', newline='') as csvfile:
    dhcpreader = csv.reader(csvfile, delimiter=',', quotechar='|')
    with open(filename, 'w') as output:
        print('writing commands to file...\n')
        for row in dhcpreader:
            try:
                command_string = f'add host name {row[0]} dns-resolving true dhcp-exclude-ip-addr on ' \
                                 f'mac-reserved-in-dhcp on mac-addr {row[1]} ipv4-address {row[2]}'
                print(command_string)
                output.write(command_string + '\n')
            except:
                errormsg = '\n#CSV PARSING ERROR: Remove any blank lines and try again'
                print(errormsg)
                exit(1)

print(f'\nSUCCESS! Commands written to {os.path.abspath(filename)}')

